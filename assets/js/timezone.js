    
    // Created for possible referencing of the clock
    var otherTime = null;

    // Initialize the clocks
    function init () {
        var localTime = new clock('local');
        localTime.run();

        otherTime = new clock('other');
        otherTime.setTimeOffset(4);
        otherTime.run();
    }

    // The continent selection handler
    function continentUpdated () {
        var continentId = $('#continents').val(), 
            cities = $("#cities");

        if (continentId != 'null') {
            cities.removeClass('hidden');

            // Load the cities for selected continent
            $.getJSON( "cities.php?id=" + continentId, function( data ) {
                // fill loaded values
                cities.empty();
                for (var i=0; i<data.length; i++) {
                    // The value is minus or plus some hours depending on the timezone
                    var value = (data[i].sign == 'plus') ? data[i].hours : -1 * data[i].hours;
                    cities.append('<option value="' + value + '">' + data[i].name + '</option>');
                }
                // select first to the clock
                cityUpdated();
            });
        } else {
            cities.addClass('hidden');
            $("#otherCityTime").addClass('hidden');
        }
    }

    // The city selection handler
    function cityUpdated () {
        var cityTimeOffset = $("#cities").val();
        $("#otherCityTime").removeClass('hidden');
        otherTime.setTimeOffset(cityTimeOffset);
    }