
class clock {

  constructor(name) {
    this.name = name;
    this.timeOffset = 0;
  }

  setTimeOffset (newTimeOffset) {
    this.timeOffset = newTimeOffset;
  }

  run () {
    var that = this;
    setInterval(function(){
      var time = that.calcTime(that.timeOffset);
      that.updateTime(time);
      that.updateClock(time);
    }, 1000);    
  }

  calcTime (offset) {

    // create Date object for current location
    var d = new Date();
    
    // convert to msec
    // add local time zone offset 
    // get UTC time in msec
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    
    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000*offset));
    
    // return time as a string
    return nd;
  }

  updateClock (date) {
    var secHand = document.getElementById(this.name + "_sec-hand").style;
    var minHand = document.getElementById(this.name + "_min-hand").style;
    var hrHand = document.getElementById(this.name + "_hr-hand").style;
    
    secHand.transform = "rotate(" + date.getUTCSeconds() * 6 + "deg)";
    minHand.transform = "rotate(" + date.getUTCMinutes() * 6 + "deg)";
    hrHand.transform = "rotate(" + (date.getUTCHours() * 30 + date.getMinutes() * 0.5) + "deg)";
  }

  updateTime (date) {
    var timeDiv = document.getElementById(this.name + "_time");
    var time = "Time for Reference --- " + date.getUTCHours() + " : " + date.getUTCMinutes() + " : " + date.getUTCSeconds();
    timeDiv.innerHTML = time;
  }  
}
