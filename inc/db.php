<?php

Class _DB {	

	private static $connection;

	private function __construct () {
	}

	public static function getConn() {
		if ( is_null( self::$connection ) ) {
			$mysqli = new mysqli('127.0.0.1', 'root', 'password', 'zuzka');

			if ($mysqli->connect_errno) {
				echo "Sorry, this website is experiencing problems.";

				echo "Error: Failed to make a MySQL connection, here is why: \n";
				echo "Errno: " . $mysqli->connect_errno . "\n";
				echo "Error: " . $mysqli->connect_error . "\n";

				exit;
			} else {

			}

			self::$connection = $mysqli;
		}
		return self::$connection;
	}

}

