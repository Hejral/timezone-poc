<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="utf-8">
        <title>Zuzka</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" href="assets/css/clock.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css" />

        <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/clock.js"></script>
        <script type="text/javascript" src="assets/js/timezone.js"></script>

    </head>


    <body onload="init();">

        <h1>Časové zóny</h1>
        <?php
        
            require_once "inc/db.php";
            require "model/continent.php";
            require "model/city.php";
            require "components/combobox.php";

            // Create continent DB object
            $continent = new Continent();

            // Print combobox with continets
            $contCombo = new Combobox('continents');
            $contCombo->setAction('continentUpdated()');
            $contCombo->addValue('null', '-- Pick one --');

            // Fill combobox with data from continent DB
            foreach ($continent->getContinents() as $continent) {
                $contCombo->addValue($continent['id'], $continent['name']);
            }
            echo $contCombo->output();

            // Print empty, hidden combobox for cities
            $cities = new Combobox('cities');
            $cities->setClass('hidden');

            // Set action to be performed when city is selected
            $cities->setAction('cityUpdated()');
            echo $cities->output();
        ?>  

        <div>
            <div id="local_time" class="time"></div><br>
            <div id="local_clock" class="clock">
              <div class="num num1"><div>1</div></div>
              <div class="num num2"><div>2</div></div>
              <div class="num num3"><div>3</div></div>
              <div class="num num4"><div>4</div></div>
              <div class="num num5"><div>5</div></div>
              <div class="num num6"><div>6</div></div>
              <div class="num num7"><div>7</div></div>
              <div class="num num8"><div>8</div></div>
              <div class="num num9"><div>9</div></div>
              <div class="num num10"><div>10</div></div>
              <div class="num num11"><div>11</div></div>
              <div class="num num12"><div>12</div></div>
              <div id="local_hr-hand" class="hr-hand"></div>
              <div id="local_min-hand" class="min-hand"></div>
              <div id="local_sec-hand" class="sec-hand"></div>
            </div>
        </div>

        <div id="otherCityTime" class="hidden">
            <div id="other_time" class="time"></div><br>
            <div id="other_clock" class="clock">
              <div class="num num1"><div>1</div></div>
              <div class="num num2"><div>2</div></div>
              <div class="num num3"><div>3</div></div>
              <div class="num num4"><div>4</div></div>
              <div class="num num5"><div>5</div></div>
              <div class="num num6"><div>6</div></div>
              <div class="num num7"><div>7</div></div>
              <div class="num num8"><div>8</div></div>
              <div class="num num9"><div>9</div></div>
              <div class="num num10"><div>10</div></div>
              <div class="num num11"><div>11</div></div>
              <div class="num num12"><div>12</div></div>
              <div id="other_hr-hand" class="hr-hand"></div>
              <div id="other_min-hand" class="min-hand"></div>
              <div id="other_sec-hand" class="sec-hand"></div>
            </div>
        </div>

    </body>
</html> 