<?php

class Continent {

	function getContinents() {
		$sql = "SELECT id, name FROM continent";
		$dbResult = _DB::getConn()->query($sql);

		if ($dbResult->num_rows === 0) {
			return "No contitnents were found";
		} else {
			$result = array();
			while ($continent = $dbResult->fetch_assoc()) {
				$result[] = $continent;
			}
			return $result;
		}
	}
}