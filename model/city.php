<?php

class City {

	function getCities() {
		$sql = "SELECT id, name FROM city";
		$dbResult = _DB::getConn()->query($sql);

		if ($dbResult->num_rows === 0) {
			return "No contitnents were found";
		} else {
			$result = array();
			while ($continent = $dbResult->fetch_assoc()) {
				$result[] = $continent;
			}
			return $result;
		}
	}

	function getCitiesByContinent($continentId) {
		$sql = "SELECT c.id, c.name, t.hours, t.sign FROM city c, timezone t WHERE c.timezone_id = t.id AND c.continent_id = ".$continentId;
		$dbResult = _DB::getConn()->query($sql);

		if ($dbResult->num_rows === 0) {
			return "No cities were found";
		} else {
			$result = array();
			while ($city = $dbResult->fetch_assoc()) {
				$result[] = $city;
			}
			return $result;
		}
	}
}