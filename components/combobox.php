<?php

class Combobox {

	private $name;
	private $class;
	private $action;
	private $values = array();

	public function __construct ($name) {
		$this->name = $name;
	}

	public function addValue ($key, $value) {
		$this->values[$key] = $value;
	}

	public function setClass ($value) {
		$this->class = $value;
	}

	public function setAction ($value) {
		$this->action = $value;
	}

	public function output () {
		$html = '';
		$html .= '<select id="'.$this->name.'"';
		$html .= ($this->class != '') ? ' class="'.$this->class.'"' : '';
		$html .= ($this->action != '') ? ' onchange="'.$this->action.'"' : '';
		$html .= '>';
		foreach ($this->values as $key => $value ) {
			$html .= '<option value="'.$key.'">'.$value.'</option>';
		}
		$html .= '</select>';
		return $html;
	}


}