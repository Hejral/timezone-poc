<?php

	require_once "inc/db.php";
	require "model/city.php";

	// Output the cities in json format
	$city = new City();

	if (isset($_GET['id'])) {
		$continentId = $_GET['id'];

		$cities = $city->getCitiesByContinent($continentId);

		echo json_encode($cities);
	} else {
		echo "[]";
	}
