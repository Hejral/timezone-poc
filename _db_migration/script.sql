create table `continent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='World continents';

create table `timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `hours` int(11) NOT NULL,
  `sign` enum ('minus', 'plus'),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='World timezones';

create table `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `continent_id` int(11) NOT NULL,
  `timezone_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  CONSTRAINT `continent_ibfk_1` FOREIGN KEY (`continent_id`) REFERENCES `continent` (`id`),
  CONSTRAINT `timezone_ibfk_1` FOREIGN KEY (`timezone_id`) REFERENCES `timezone` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coties accross the world';

insert into continent (`name`) values ('europe');
insert into continent (`name`) values ('asia');
insert into continent (`name`) values ('america');

insert into timezone (`name`, `hours`, `sign`) values ('Paris', 2, 'plus');
insert into timezone (`name`, `hours`, `sign`) values ('Moscow', 3, 'plus');
insert into timezone (`name`, `hours`, `sign`) values ('New York', 4, 'minus');
insert into timezone (`name`, `hours`, `sign`) values ('Vancouver', 6, 'minus');

insert into city (`name`, `continent_id`, `timezone_id`) values ('Paris', 1, 1);
insert into city (`name`, `continent_id`, `timezone_id`) values ('Prague', 1, 1);
insert into city (`name`, `continent_id`, `timezone_id`) values ('Moscow', 2, 2);
insert into city (`name`, `continent_id`, `timezone_id`) values ('New York', 3, 3);
insert into city (`name`, `continent_id`, `timezone_id`) values ('Washington', 3, 3);
insert into city (`name`, `continent_id`, `timezone_id`) values ('Calgary', 3, 4);
insert into city (`name`, `continent_id`, `timezone_id`) values ('Vancouver', 3, 4);
insert into city (`name`, `continent_id`, `timezone_id`) values ('L.A.', 3, 4);
